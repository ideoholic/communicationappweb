import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageserviceService {
  public arrayOFSelectedUsers = [];

  public name:String;
  public username:String
  public userid:String
  public password:String
  public contact:String
  public id:String
  public isAdmin:boolean
  public group:String

  constructor() { }

  setSelectedUsers(GetAnArray){
    this.arrayOFSelectedUsers = GetAnArray;
  }
  
  getSelectedUsers(){
    return this.arrayOFSelectedUsers;
  }

  setArrayEmpty(){
    this.arrayOFSelectedUsers.splice(0,this.arrayOFSelectedUsers.length);
  }

  setAdminDetails(response){
    this.name = response.name;
    this.username = response.username;
    this.contact= response.contact;
    this.userid = response.userid;
    this.password = response.password;
    this.group =  response.group;
    this.id = response.id;
    this.isAdmin = response.isAdmin;
  }

  getAdminDetails(){
    return [ this.username,this.group,this.name,this.contact,this.userid,this.password,this.id,this.isAdmin]
  }
}
