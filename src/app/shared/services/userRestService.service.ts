import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Globals } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserRestService {
   
   constructor(private http: HttpClient) { }

   loginUser(user){
	  let loginUrl = Globals.APP_URL + 'login';
      return this.http.post<any>(loginUrl, user, {observe: 'response'});
   }
   
    getAllUsers(){
      let requestURL = Globals.APP_URL+ 'users' ;
      return this.http.get<any>(requestURL );
   }
   
    userDelete(id){
		console.log("deletion of user");
		 let deleteUrl = Globals.APP_URL + id;
		 return this.http.delete<any>(deleteUrl);
		
	}
	
	getUser(id)
	{
		console.log("userid is " +id);
		 let userUrl = Globals.APP_URL +id ;
		 return this.http.get<any>(userUrl);
  	}

  getSearchedUser(group){
     console.log("I am In Service Paw");
     let userUrl = Globals.APP_URL +"?group="+group;
     return this.http.get<any>(userUrl);
  }
  
   saveUser(user)
   {
	   let newuserUrl = Globals.APP_URL + 'register';
	   return this.http.post<any>(newuserUrl, user, {observe: 'response'});
	   
   }
   
   getUserGroup(group)
	{
		console.log("groupname is  " +'group' + group);
				console.log("groupname in rest methodcall is  " +group);

		 let userGroupUrl = Globals.APP_URL + 'group/'+group ;
		 console.log(this.http.get<any>(userGroupUrl));
		 return this.http.get<any>(userGroupUrl);

	}
	
	deleteAll()
	{
		console.log("deletion of all users");
		 let deleteAllUrl = Globals.APP_URL + 'users';
		 return this.http.delete<any>(deleteAllUrl);
	}

   getDetails(){
	let userGroupUrl = Globals.APP_URL + '/username' ;
	return this.http.get<any>(userGroupUrl);
   }

   updateDetails(details){
	console.log(details.adminName+" "+  details.password+" "+ details.id+" "+details.userId);
	let userGroupUrl = Globals.APP_URL + 'update' ;
	return this.http.post<any>(userGroupUrl,details, {observe: 'response'}); 
   }

   ResendSms(sms){
	let userGroupUrl = Globals.APP_URL + 'resend' ;
	return this.http.post(userGroupUrl,sms,{responseType: 'text'});
   }
	
}

