import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material';
import { MessageRestService } from '../../shared/services/messageRestService.service';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/guard/auth.service';



export class MessageElement {
    position: number;
    subject: String;
    description: string;
    group: string;
    time: string;
    date: string;
    userid: string;
    delete: string;
    id: string;
}



@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
   
    displayedColumns = ['position','view','subject','group','date','time','delete'];
    dataSource = new MatTableDataSource ;
    places: Array<any> = [];

    @ViewChild(MatSort, {static: true})
    sort: MatSort;
    
    myObj:MessageElement;
    
    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches    
    }
   
    constructor(private messageService: MessageRestService,private router: Router,
        private authService: AuthService) {}
      
        ngOnInit() {
           
            this.getMessages();
            
           }
       
    
           onDelete(messageId:string){
            console.log("Message Id is = "+messageId);
            this.messageService.deleteAMessage(messageId)
           .subscribe(response => {
              // 2. Remove deleted element by using the ID from ELEMENT_DATA and reload the
             this.getMessages();
            }, error =>{});
        }

        getMessages(){
            var ELEMENT_DATA: MessageElement[] = [];
          
            this.messageService.getAllMessages('1')
            .subscribe(
              response => {                 
                if(response.length == 0){
                    this.dataSource = new MatTableDataSource(ELEMENT_DATA);
                    return;
                }

                 var counter = 0;
                     
                 response.forEach(element => {
                     this.myObj = new MessageElement();
                     this.myObj.position = ++counter;
                     this.myObj.date = element.date;
                     this.myObj.time = element.time;
                     this.myObj.subject = element.subject;
                     this.myObj.group = element.group;
                     
                     this.myObj.id= element.id;

                     ELEMENT_DATA.push(this.myObj);
                    });
                    this.dataSource = new MatTableDataSource(ELEMENT_DATA);
                    this.dataSource.sort = this.sort;
              },
              error => console.log(error)
            );
        }
        onView(messageId:string){
            this.messageService.getASingleMessage(messageId)
            .subscribe(
                response=>{
                    this.router.navigate(['/viewmessage',messageId]);
                },
                error => console.log(error)
            );

        }
}
