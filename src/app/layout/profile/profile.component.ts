import { Component, OnInit, } from '@angular/core';
import { UserRestService } from '../../shared/services/userRestService.service';
import {ActivatedRoute,Params} from '@angular/router';
import { StorageserviceService } from '../../shared/services/storageservice.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  hide: boolean = false;

  public obj = {
    name:"",
    contact:"",
    userid:"",
    password:"",
    isAdmin:"",
    id:"",
    group:"",
    username:""
  };
  public adminDetails=[];

  public username="";
  public name="";
  public contact="";
  public userid="";
  public password="";
  public isAdmin="";
  public id="";
  public group="";
  
  constructor(private userService: UserRestService,private storageServices :StorageserviceService,
     private storageData:StorageserviceService,
    private route:ActivatedRoute) { }

  ngOnInit() {
    this.adminDetails = this.storageData.getAdminDetails();
    console.log(this.adminDetails);
    this.username = this.adminDetails.reverse().pop();
    this.group = this.adminDetails.pop();
    this.name = this.adminDetails.pop();
    console.log(this.name);
    this.contact = this.adminDetails.pop();
    this.userid = this.adminDetails.pop();
    this.password = this.adminDetails.pop();
    this.id= this.adminDetails.pop();
    this.isAdmin = this.adminDetails.pop();
  }

  updateVlaues(){
    console.log(this.name+" "+  this.password+" "+ this.id+" "+this.userid);
    this.obj.name = this.name;
    this.obj.contact = this.contact;
    this.obj.userid = this.userid;
    this.obj.password = this.password;
    this.obj.group = this.group;
    this.obj.username = this.username;
    this.obj.isAdmin = this.isAdmin;
    this.obj.id =this.id;
    console.log(this.obj.name+" "+  this.password+" "+ this.id+" "+ this.obj.userid);
    this.userService.updateDetails(this.obj)
    .subscribe(
      response=>{
       this.storageServices.setAdminDetails(response.body);
         //Add toast
       var x = document.getElementById("snackbar");
       x.className = "show";
       setTimeout(function(){ 
         x.className = x.className.replace("show", ""); }, 3000);
      },
      error=>{console.log(error)}
    );
  }

}
