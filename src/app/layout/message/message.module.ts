import { CommonModule } from '@angular/common';
import { NgModule} from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCardModule, MatIconModule, MatTableModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material';
import { FormsModule } from '@angular/forms';

import { MaterialModuleLoader } from '../../material-module';
//import { MessageComponent } from './message.component';

@NgModule({
  imports: [
        CommonModule,
        MatGridListModule,
        MatCardModule,
        MatTableModule,
        MatButtonModule,
        FormsModule,
        MatIconModule,
        MatFormFieldModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
  ],
  declarations: []
})
export class MessageModule { }
