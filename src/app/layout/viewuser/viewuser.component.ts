import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Params} from '@angular/router';
import { UserRestService } from '../../shared/services/userRestService.service';
import { UsersElement } from '../users/users.component';
import { MatTableDataSource } from '@angular/material';
import { GroupRestServices } from '../../shared/services/groupRestServices.service';

export class GroupsElement{
  
}

@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.scss']
})
export class ViewuserComponent implements OnInit {
  hide: boolean = false;

    userDetail={
	    name : '',
      username: "",
      password : "",
	    contact : "",
      group: "",
      id : ""
    }

  constructor(private userService: UserRestService,  private groupService: GroupRestServices
    ,private route:ActivatedRoute) { }
	
  groupObj = new GroupsElement();
  groupArray =[];

  ngOnInit() {
    let id = this.route.snapshot.params['id']
    this.userDetail.id = id;
    this.FunCall(id);
  
   this.groupService.getGroups()
    .subscribe(response=>{
      response.forEach(element => {
        this.groupObj= element.groupName;
        this.groupArray.push(this.groupObj);
      });
  
    },
    error => console.log(error)
   );
  }
  
  FunCall(userid:string){
    var myObj:UsersElement;
    var ELEMENT_DATA: UsersElement[] = [];
	
    this.userService.getUser( this.userDetail.id)
            .subscribe(
                response=> {
					this.userDetail.name=response.name;         
          this.userDetail.group = response.group;
          this.userDetail.contact = response.contact;
					 this.userDetail.username= response.username;
           this.userDetail.password = response.password;
                },
                error => console.log(error)
              );  
            
   }

   update(){

    this.userService.updateDetails(	this.userDetail)
    .subscribe(
      response=>{
        var x = document.getElementById("snackbar1");
        x.className = "show";
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
      },
		err => {
		  var x = document.getElementById("snackbar2");
         x.className = "show";
         setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3500);
		
		}
    );
  }
}
 