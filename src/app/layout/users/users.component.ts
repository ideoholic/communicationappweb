import { Component, OnInit, ViewChild, HostListener,ViewEncapsulation } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material';
import { UserRestService } from '../../shared/services/userRestService.service';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/guard/auth.service';
import {ActivatedRoute,Params} from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { StorageserviceService } from '../../shared/services/storageservice.service';
import { element } from '../../../../node_modules/protractor';
import { NgForm } from '@angular/forms' 

export class UsersElement {
	check:boolean;
    parentname: string;
    position: number;
    contact: string;
    group: string;
    userid: string;
	delete: string;
	view: string;
	id : string;
	isAdmin : boolean;
	resendSms:String;
}

@Component({
    selector: 'app-users,App',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})	
export class UsersComponent implements OnInit {

	userData ={
		 username :"",
		 userid: "",
		 password : "",
		 contact : "",
		 group: "",
		 id : "",
		 name:""
   }

	form: FormGroup;
	ordersData = [];
	bool:boolean;

    parentname: string;
    displayedColumns = ['check','position', 'view','parentname', 'contact', 'userid', 'group','delete','resendSms'];
	dataSource = new MatTableDataSource([]);
    
    @ViewChild(MatSort, {static: true}) 
	     sort: MatSort;
		 myObj:UsersElement;
	
	@ViewChild('tag', {static: true}) 
	elementSearch;	 

    applyFilter(filterValue: string) {
        filterValue = filterValue.trim(); // Remove whitespace
        filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matche
	}

	count:number=0;
	arrayOfCheckMarks=[];
	size:number=0;
	returnValue:boolean=true;
	xyz:string="";

	@ViewChild('tag',{static:false})
	searchForm : NgForm;
	
     
	constructor(private userService: UserRestService,
	private router: Router,private storageService:StorageserviceService,
        private authService: AuthService, private route: ActivatedRoute) {}

	//WILL CALL METHOD TO DISPLAY ALL MESSAGES IN DATABASE		
    ngOnInit() {
		this.displayUsers();	
	}

	//Event Listener
	@HostListener('document:keypress', ['$event'])
	handleKeyboardEvent(event: KeyboardEvent) { 
	  let key = event.key;
	  let searchText = this.searchForm.value.searchText;
	  if(key === "Enter"){
		  if(searchText === undefined || searchText === null || searchText === ""){
			  this.displayUsers();
		  }else{
			  this.onSearch(searchText);
		  }
	  }
	  
	}

	//WILL DISPLAY MESSAGES BASED ON SEARCH
	onSearch(valueNew){
		if(valueNew === undefined || valueNew===null || valueNew==="" ){
			this.displayUsers();
		}
		else{
		this.userService.getSearchedUser(valueNew)
		.subscribe(response=>{
			this.displayMessages(response);
		})
	  }	 
	}
	
	//WILL MAKE MESSAGE-SHOW BUTTON VISIBLE/INVISIBLE BASED ON CHECKBOX VALUE
	// VIA count and boolean variable
	onCheck(checkID){
	   this.size=this.arrayOfCheckMarks.length;
	   if(this.size == 0){
		this.arrayOfCheckMarks.push(checkID); 
		this.count++;
	   }
       else{
		this.returnValue = this.arrayOfCheckMarks.includes(checkID);
			if(!this.returnValue){
				this.arrayOfCheckMarks.push(checkID);
				this.count++;
			}else{
				var index = this.arrayOfCheckMarks.indexOf(checkID);
	
				if (index > -1) {
					this.arrayOfCheckMarks.splice(index, 1);
					}
				this.count--;	
			}
	   }   
	}

	//WILL SAVE THE OBJECTS BASED ON CHECK BOX IN THE STOARAGE SERVICE 
	saveIn(){
		console.log(this.arrayOfCheckMarks);
        this.storageService.setSelectedUsers(this.arrayOfCheckMarks);
		this.router.navigate(['/addmessage']);
	}

	//WILL DELETE THE USERS BASED ON CHECK BOX IN THE STOARAGE SERVICE 
	deleteUsers(){
		  var size= this.arrayOfCheckMarks.length;

		  for(var i=0;i<size;i++){
			this.deleteUser(this.arrayOfCheckMarks[i]);
		  }
		  this.arrayOfCheckMarks=[];
		  this.count=0
		   
	}
	
	//WILL DISPLAY ALL USERS PRESENT IN THE DATABASE
	displayUsers(){
            this.userService.getAllUsers()
            .subscribe(response => {
				this.displayMessages(response);
              },
              error => console.log(error)
			);
	}
	
	array=[
		
	];

	//BUSINESS LOGIC TO DISPLAY MESSAGES
	displayMessages(response){
				let ELEMENT_DATA: UsersElement[]= [];
                console.log("Response from server " + response);
				
				let counter=0,i=0;
				response.forEach(element=>
				{
					this.myObj=new UsersElement();
					this.myObj.parentname= element.name;
					this.myObj.position= ++counter;
				    this.myObj.userid= element.username;
					this.myObj.contact= element.contact;
					this.myObj.group= element.group;
					this.myObj.id = element.id;

					ELEMENT_DATA.push(this.myObj);
					this.array.push(ELEMENT_DATA);
				});

				this.dataSource = new MatTableDataSource(ELEMENT_DATA);
				this.dataSource.sort = this.sort;
				
		}
		
		//WILL DELETE THE USER BASED ON USERS ID[PRIMAERY-ID]
		deleteUser(id : string){
		this.userService.userDelete(id)
	 		.subscribe(response=> {
				this.displayUsers(); 
	 			},
				error => console.log(error)
			);

	    }
		
		//WILL DISPLAY WHOLE THINK RELATED TO PARTICULAR USER
		onView(id:string){
			console.log("this is id"+id);
            this.userService.getUser(id)
            .subscribe(
                response=>{
					this.router.navigate(['/viewuser', id ]);
					},
                error => console.log(error)
            );
		}
		
		//WILL SEND USERNAME AND PASSWORD TO ALREADY RERGISTERED USER(PARENT) 
		onSend(id:String){
			this.userService.getUser(id)
            .subscribe(
                response=>{
					this.userData.username = response.username;
					this.userData.password = response.password;
					this.userData.id = response.id;
					this.userData.name = response.name;
					this.userData.contact = response.contact;
					this.userData.group = response.group;

					this.onResend(this.userData);
				},
                error => console.log(error)
            );	
		}

		onResend(objResend){
			this.userService.ResendSms(objResend)
			.subscribe(
				res=>{
					},
				error=>{
					console.log(error);
				}
			);
		}
		
}

