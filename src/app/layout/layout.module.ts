import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {MatSortModule} from '@angular/material/sort';
import {
    
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatCardModule,
    MatTableModule,
    MatFormFieldModule,
    MatDatepickerModule, 
    MatNativeDateModule
} from '@angular/material';
import { CdkTableModule } from '@angular/cdk/table';
import { TranslateModule } from '@ngx-translate/core';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { NavComponent } from './nav/nav.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { GroupsComponent } from './groups/groups.component';
import { MessageComponent } from './message/message.component';
import { NewuserComponent } from './newuser/newuser.component';
import { DisplaymessageComponent } from './displaymessage/displaymessage.component';
import { ViewuserComponent } from './viewuser/viewuser.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { TopnavComponent } from './components/topnav/topnav.component';
import { ProfileComponent } from './profile/profile.component';

@NgModule({
    imports: [
        MatInputModule,
        MatTableModule,
        MatSortModule,
        MatCheckboxModule,
        CommonModule,
        LayoutRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatListModule,
        MatCardModule,
        MatTableModule,
        MatDatepickerModule, 
        MatNativeDateModule,
        MatFormFieldModule,
        CdkTableModule,
        TranslateModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [LayoutComponent,DisplaymessageComponent, NavComponent, TopnavComponent, SidebarComponent, DashboardComponent, UsersComponent, GroupsComponent, MessageComponent, NewuserComponent, ViewuserComponent, ProfileComponent ]

})
export class LayoutModule { }
