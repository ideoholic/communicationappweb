import { Component, OnInit ,ViewChild} from '@angular/core';
import { NgForm } from '@angular/forms' 
import {ActivatedRoute,Params} from '@angular/router';
import { UserRestService } from '../../shared/services/userRestService.service';
import { MatTableDataSource } from '@angular/material';
import { GroupRestServices } from '../../shared/services/groupRestServices.service';

export class GroupsElement{
  
}
@Component({
  selector: 'app-newuser',
  templateUrl: './newuser.component.html',
  styleUrls: ['./newuser.component.scss']
})
export class NewuserComponent implements OnInit {
  hide: boolean = false;
     
	   newuserData ={
	   name :"",
     username: "",
        password : "",
	    contact : "",
        group: "",
	    id : ""
  }
  
  isNameEmpty:boolean = false;
  isUserNameEmpty:boolean = false;
  isPasswordEmpty:boolean = false;
  isContactEmpty:boolean = false;
  isGroupEmpty:boolean = false;
  
  @ViewChild('tag',{static:false})
  enteredValues : NgForm;

  constructor(private userService: UserRestService, 
    private route:ActivatedRoute , private groupService: GroupRestServices) { }  
	
  groupArray =[];
 
  groupObj = new GroupsElement();

  ngOnInit() {
    this.groupService.getGroups()
    .subscribe(response=>{
      response.forEach(element => {
        this.groupObj= element.groupName;
        this.groupArray.push(this.groupObj);
      });
    },
    error => console.log(error)
   );
  }

  
  userCreated(){
    var flag:boolean = false;

    if(this.newuserData.name === "" || this.newuserData.name === null||this.newuserData.name === undefined){
      this.isNameEmpty = true;
       flag = true;
    }else{
      this.isNameEmpty = false;
    }

    if(this.newuserData.username === "" || this.newuserData.username === null||this.newuserData.username === undefined){
      this.isUserNameEmpty = true; flag = true;
    }else{
      this.isUserNameEmpty = false; 
    }

    if(this.newuserData.password === "" || this.newuserData.password === null||this.newuserData.password === undefined){
      this.isPasswordEmpty = true; flag = true;
    }else{
      this.isPasswordEmpty = false;
    }

    if(this.newuserData.contact === "" || this.newuserData.contact === null||this.newuserData.contact === undefined){
      this.isContactEmpty = true;
      flag = true;
    }else{
      this.isContactEmpty = false;
    }

    if(this.newuserData.group === "" || this.newuserData.group === null||this.newuserData.group === undefined){
      this.isGroupEmpty = true;
      flag = true;
    }else{
      this.isGroupEmpty = false;
    }

    if(flag)
    return;

	  this.userService.saveUser(this.newuserData)
	  .subscribe(
		res => {
       //Clear the form field
       this.enteredValues.resetForm();
         //Add toast
         var x = document.getElementById("snackbar1");
         x.className = "show";
         setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
		},
		err => {
		  var x = document.getElementById("snackbar2");
         x.className = "show";
         setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
		}
    );   	    
}

}