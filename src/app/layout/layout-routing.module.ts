import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileComponent } from './profile/profile.component'
import { LayoutComponent } from './layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { GroupsComponent } from './groups/groups.component';
import { MessageComponent } from './message/message.component';
import { NewuserComponent } from './newuser/newuser.component';
import { DisplaymessageComponent } from './displaymessage/displaymessage.component';
import { ViewuserComponent } from './viewuser/viewuser.component';


const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                redirectTo: 'dashboard'
            },
            {
                path: 'dashboard',
                component: DashboardComponent
                //loadChildren: './dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'users',
                component: UsersComponent
                //loadChildren: './users/users.module#UsersModule'
            },
            {
                path: 'groups',
                component: GroupsComponent
		//loadChildren: './groups/groups.module#GroupsModule'

            },
            {
                path: 'addmessage',
                component: MessageComponent
		//loadChildren: './message/message.module#MessageModule'
            },
            {
                path: 'adduser',
                component: NewuserComponent
		//loadChildren: './newuser/newuser.module#NewuserModule'
            },
            {
               path: 'viewmessage/:id',
               component: DisplaymessageComponent
       //loadChildren: './message/message.module#MessageModule'
           },
            {
               path: 'viewuser/:id',
               component: ViewuserComponent
       //loadChildren: './message/message.module#MessageModule'
           },
           {
            path: 'profile',
            component: ProfileComponent
    //loadChildren: './message/message.module#MessageModule'
        }
		   
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class LayoutRoutingModule {}
