import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material';
import { MatTableDataSource } from '@angular/material';
import { GroupRestServices } from '../../shared/services/groupRestServices.service';

export class GroupsElement {
  position: number;
  group: String;
}
  

@Component({
    selector: 'app-groups',
    templateUrl: './groups.component.html',
    styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
  displayedColumns: string[] = ['position', 'name'];
  dataSource = new MatTableDataSource ;
  groupObj = new GroupsElement();

  @ViewChild(MatSort, {static:true}) sort: MatSort;
  
  constructor(private groupService:GroupRestServices) {
   }


  ngOnInit() {
    this.getGroups();
    this.dataSource.sort = this.sort;
  }
 
  getGroups(){
    var ELEMENT_DATA: GroupsElement[] = [];
    var counter = 0;

    this.groupService.getGroups()
    .subscribe(response=>{
      response.forEach(element => {
        this.groupObj = new GroupsElement();
        this.groupObj.position = ++counter;
        this.groupObj.group= element.groupName;
       
        ELEMENT_DATA.push(this.groupObj);
      });
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSource.sort = this.sort;
    },
    error => console.log(error)
   );
  }

}
