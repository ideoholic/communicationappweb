import { CommonModule } from '@angular/common';
import { NgModule} from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule, MatCardModule, MatIconModule, MatTableModule } from '@angular/material';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatFormFieldModule } from '@angular/material';

import { StatModule } from '../../shared/modules/stat/stat.module';
import { GroupsRoutingModule } from './groups-routing.module';
// import { GroupsComponent } from './groups.component';

@NgModule({
    imports: [
        CommonModule,
        GroupsRoutingModule,
        MatGridListModule,
        StatModule,
        MatCardModule,
        MatTableModule,
        MatButtonModule,
        MatIconModule,
        MatFormFieldModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: []
})
export class GroupsModule {}
