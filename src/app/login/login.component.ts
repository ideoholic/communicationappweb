import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../shared/guard/auth.service';
import { UserRestService } from '../shared/services/userRestService.service';
import { StorageserviceService } from '../shared/services/storageservice.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    loginUserData = {username:"", password:""};
	headers = {};
	isPasswordEmpty:boolean = false;
	isUserNameEmpty:boolean = false;

    constructor(private router: Router,private storageServices :StorageserviceService,
                private userService: UserRestService,
		private authService: AuthService) {}

    ngOnInit() {}

    onLogin() {
		if(this.loginUserData.password === null || this.loginUserData.password===undefined 
		|| this.loginUserData.password === "" ){
			this.isPasswordEmpty = true;
			return;
		}
   
		if(this.loginUserData.username === null || this.loginUserData.username === undefined
			|| this.loginUserData.username === ""){
			this.isUserNameEmpty = true;
			return;
		   }

		   this.isUserNameEmpty = false;
		   this.isPasswordEmpty = false;
	this.userService.loginUser(this.loginUserData)
	   .subscribe(res => {	

		const keys = res.headers.keys();
	
		this.headers = keys.map(key =>
		`${key}: ${res.headers.get(key)}`);
	
		 const token = res.body.token;

		   this.authService.setToken(token);

		   this.getAdminDetails();

		   this.router.navigate(['/dashboard']);
		   return;
		},
		err => {
		console.log(err);
		this.router.navigate(['/login']);

		var x = document.getElementById("snackbar");
		x.className = "show";
		setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
   
	    }
	 );
	}


	getAdminDetails(){
		this.userService.getDetails()
		.subscribe(res=>{
			this.storageServices.setAdminDetails(res);
		},
			err =>{console.log(err);}
		);
	}
}
